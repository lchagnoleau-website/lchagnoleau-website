---
title: "FreeRTOS : configASSERT( xTaskToNotify )"
date: 2021-01-17T10:00:00+01:00
draft: false
---

# FreeRTOS : configASSERT( xTaskToNotify )
En développant mon application utilisant le superbe FreeRTOS, j'ai eu la surprise de tomber dans un _assert_ après avoir activé le système de debug SEGGER SystemView. Mais cela peut aussi arriver si les fréquences d'horloge du CPU sont trop faible.
## Comprendre le problème

Lorsque l'on active le SEGGER SystemView avec une fréquence d'horloge du CPU trop faible, nous tombons dans un _assert_.  
Cette _assert_ est déclenchée car une tache est notifiée à partir d'une interruption mais cette tâche à un id de 0. Ce qui ne devrait pas être le cas.  
Cela se produit très certainement du fait que l'interruption en question se déclenche, et donc notifie une tâche, avant même que les tâches ne soient créée et initialisée.

Lorsque l'on active le SystemView, des instructions sont rajoutées à des endroits stratégique du kernel afin d'envoyer certaine trace de debug. Notamment, dans la fonction Systick qui s’exécute très régulièrement. De ce fait, la partie du code ou se fait l'initialisation des tâches se fait bien plus tard que qu'avant. Or, dans notre application, les interruptions sont lancées bien avant l'initialisation des tâches.  
Je résume brièvement ce que fait le CPU au début du main **sans SystemView** :

1.  Initialisation des horloges
2.  Initialisation des périphériques
3.  Initialisation des interruptions
4.  Initialisation des taches
5.  Une interruption se déclenche et notifie une tâche

Dans ce cas tout va bien car l'interruption se déclenche après l'initialisation des tâches. Mais dans le cas ou les horloges CPU sont plus faible et/ou SystemView est activé, notre historique devient comme suit :

1.  Initialisation des horloges
2.  Initialisation des périphériques
3.  Initialisation des interruptions
4.  **Une interruption se déclenche et notifie une tâche**
5.  Initialisation des taches

En effet, à cause du temps rajouté entre le moment ou les interruptions sont activée et le moment ou les tâche sont initialisée, une interruption s'est déjà déclenchée.  
Dans ce cas particulier, une tâche est donc notifiée avant que cette dernière ne soit initialisée ce qui cause le fameux configASSERT( xTaskToNotify ).

## Solution

La solution la plus simple et la plus sûre consiste à initialiser les interruptions après les tâches, tout simplement. Ce qui donne le déroulement suivant :

1.  Initialisation des horloges
2.  Initialisation des périphériques
3.  Initialisation des taches
4.  Initialisation des interruptions
5.  Une interruption se déclenche et notifie une tâche