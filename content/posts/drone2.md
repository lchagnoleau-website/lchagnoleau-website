---
title: "Drone software from scratch 2/x"
date: 2021-02-13T13:53:10+01:00
draft: false
---
## Comment fonctionne un drone

Un drone par définition est un objet volant sans pilote à bord. Il peut être sous différentes forme mais le drone qui nous intéresse est le Quadcopter :

{{< image src="/drone2/drone-quad.jpg" alt="Exemple d'un drone à 4 moteurs" position="center" style="border-radius: 8px;" >}}

Nous allons tâcher de comprendre au long de cet article le fonctionnement de ce type de drone et particulièrement comment il fait pour rester stable.

### Composition d'un drone

#### Les moteurs et les hélices

Pour qu'il ne tourne pas sur lui même, le drone dispose de 4 moteurs dont les sens de rotations sont symétriquement opposées comme sur le schéma ci-dessous :

{{< image src="/drone2/rotation.png" alt="Rotation des hélices" position="center" style="border-radius: 8px;" >}}

Ainsi, le drone reste stable et ne tourne pas sur lui même.
Notez également que pour les hélices, l'inclinaison des pâles est inversée en fonction du sens de rotation du moteur sur lequel elles sont fixées.

#### L'ESC

L'ESC pour **Electronic speed control** est le composant faisant la liaison entre le micro-contrôleur est les moteurs. Il reçoit généralement en entrée une PWM et s'occupe de faire tourner les moteurs.
Il en faut un par moteur.

{{< image src="/drone2/esc.jpg" alt="ESC" position="center" style="border-radius: 8px;" >}}

#### Le BEC

Le **Battery Eliminator Circuit** sert à distribuer les alimentions nécessaire aux différents composants du drone.

#### Le récepteur radio

Il permet de recevoir les commandes envoyées par une radio externe. Il envoie ensuite ces commandes au micro-contrôleur.

{{< image src="/drone2/x4r.jpg" alt="radio" position="center" style="border-radius: 8px;" >}}

#### Les capteurs

##### L'accéléromètre

Il mesure l'accéleration (en m/s² ou en G ( G = 9.81m/s²)) sur les 3 axes x, y et z. Grâce à lui, nous sommes capable de connaître avec précision l'inclinaison du drone en temps réel.
Par exemple, lorsque le drone est posé à plat, son accélération sur l'axe y est de 1G (l'attraction terrestre) et 0 sur les autres axes.

##### Le gyroscope

Il mesure la rotation du drone sur les 3 axes (en rad/s ou deg/s). Quand le drone est parfaitement stable, on devrait mesurer 0 rad/s sur chacun des 3 axes. En revanche, si il est en train d'effectuer un rotation, celle-ci sera mesurée grâce au gyroscope

#### Le Micro-contrôleur

C'est lui qui va envoyer les bonnes commandes aux ESC en fonction des données de la radio et des capteurs.

{{< image src="/drone2/f405.jpg" alt="matek" position="center" style="border-radius: 8px;" >}}

### La stabilité

Pour conserver sa stabilité, le drone doit faire tourner ses hélices à la même vitesse, ou presque. Déjà, elles doivent tourner suffisamment vite pour que le drone ne tombe pas, mais pas trop vite non plus pour ne pas que le drone ne prenne de la hauteur.
Cependant, il y a  plusieurs facteurs qui font que le drone va partir à la dérive. Ces facteurs sont : le moindre déséquilibre de poids (1g plus lourds à droite qu'à gauche par exemple), les courants d'air, les différences de vitesse infime des moteurs liès aux tolérances de chaque composants...

Il faut donc sans cesse connaître la l'état du drone et adapté la rotation des moteurs en fonctions. Cette tâche est réalisée par la PID (**proportionnel, intégral, dérivé**). On verra comment elle fonctionne en détail dans un article dédié. Pour faire simple, il s'agit d'un algorithme qui prend en entrée : la commande actuelle des moteurs et les données de l'accéléromètre et du gyroscope. Elle calcul gràce à ces données des nouvelles consignes pour les moteurs. Et elle tourne en boucle aussi souvent que possible (plusieurs milliers de fois par secondes).

## Le drone utilisé pour le projet

Voici la base sur laquelle nous allons réaliser le logiciel embarqué :

{{< image src="/drone2/mydrone.jpg" alt="Mon drone" position="center" style="border-radius: 8px;" >}}

### Liste des composants

Je vous liste ici les composants utilisés :

|Composant|Référence|
|--|--|
| Contrôleur de vol| [Matek F405 STD](http://www.mateksys.com/?portfolio=f405-std) |
|Micro-contrôleur|[STM32F405RGT6](https://www.st.com/en/microcontrollers-microprocessors/stm32f405rg.html)|
|Gyro/Accéléromètre|[ICM-20602](https://invensense.tdk.com/products/motion-tracking/6-axis/icm-20602/)|
|ESC|[Racerstar RS20A Lites](https://www.racerstar.com/racerstar-rs20a-lites-20a-blheli_s-16_5-bb2-2-4s-brushless-motor-support-dshot600-for-rc-fpv-racing-drone-p-72.html)|
|Récepteur radio|[FrSky X4R](https://www.frsky-rc.com/product/x4r/)|

Je ne liste pas les autres composants du drone car nous n'en auront pas besoin pour le projet.
  
### Documentations

- [ICM-20602 Software User Guide](https://3cfeqx1hf82y3xcoull08ihx-wpengine.netdna-ssl.com/wp-content/uploads/2015/12/eMD_Software_Guide_ICM20602.pdf)
- [ICM-20602 Datasheet](https://3cfeqx1hf82y3xcoull08ihx-wpengine.netdna-ssl.com/wp-content/uploads/2020/11/DS-000176-ICM-20602-v1.1.pdf)
- [STM32F405xx Datasheet](https://www.st.com/resource/en/datasheet/stm32f405rg.pdf)
- [STM32F405xx Reference manual](https://www.st.com/resource/en/reference_manual/dm00031020-stm32f405415-stm32f407417-stm32f427437-and-stm32f429439-advanced-armbased-32bit-mcus-stmicroelectronics.pdf)
- [STM32F405xx Programming manual](https://www.st.com/resource/en/programming_manual/dm00046982-stm32-cortexm4-mcus-and-mpus-programming-manual-stmicroelectronics.pdf)
- [Matek F405 STD Pinout](http://www.mateksys.com/?portfolio=f405-std#tab-id-4)
- [FrSky X4R Manual](https://www.frsky-rc.com/wp-content/uploads/2017/07/Manual/X4R%20X4RSB%20CPPM.pdf)
