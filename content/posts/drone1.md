---
title: "Drone software from scratch 1/x"
date: 2021-02-06T19:27:42+01:00
draft: false
---

Cela fait quelques temps que j'ai envie de me lancer ce défi. Je suis fan de drone et je m'en suis déjà fabriqué deux.
Ils tournent avec [Betaflight](https://github.com/betaflight/betaflight), un fork de [Cleanflight](https://github.com/cleanflight/cleanflight). Il doit s'agir je pense des logiciels libre pour drone les plus utilisés.
Le projet consiste à de recoder from scratch un logiciel embarqué pour un de mes drone. Je ne ferai jamais aussi bien qu'eux mais l'idée est plutôt de vous montrer comment on peut réaliser un projet ambitieux depuis zéro. En effet, même si cela est possible, je ne compte pas pomper le code de Cleanflight ni de m'en inspirer.
A première vu, le projet semble démesuré. Il y a tellement de chose à faire (drivers accéléromètre, gyroscope, gestion des moteurs, PID...) que la tâche peut vite sembler insurmontable.
L'idée, pour s'en sortir, consiste à découper le projet en différentes tâches. Vous verrez qu'après les avoir clairement définies, on y verra beaucoup plus clair et l'objectif nous semblera réalisable.
La première partie de ce projet commence maintenant et l'objectif pour aujourd'hui est de planifier et lister les tâches à accomplir pour faire voler notre drone.
## Cahier des charge

Chaque projet ce doit de commencer par une définition claire et précise des spécifications :

1. Le drone doit pouvoir voler

2. Le drone doit pouvoir être pilotable par radio-commande

    2.1 Une commande des gaz

    2.2 Une commande pour le roulis

    2.3 Une commande pour le lacet

    2.4 Une commande pour le tangage

    2.5 Une commande pour armer/désarmer les moteurs

3. Le drone doit pouvoir rester stable en vol lorsque aucune commande n'est envoyée

4. Lorsque les commandes de tangage et/ou roulis et/ou lacet revienne à zéro, le drone doit se remettre à l'horizontale et se stabiliser

5. Le drone ne doit pas dépasser une inclinaison maximale de 30°

Nous n'avons pas pour vocation de recoder toutes les fonctionnalités que propose des logiciels comme Cleanflight ou Ardupilot. Ici, pas de GPS, de gestion multi-board, gestion des différents modes de vol, atterrissage automatique ou autre. On va rester simple histoire que le projet ne prenne pas 20 ans.
Notez que la liste des spécifications peut être amenée à évoluer et c'est normal. Tout au long dur projet, on devra challenger cette liste. On va s'apercevoir qu'une spécification n'est pas assez précise et qu'il faut la découper, ou bien que deux d'entre elles sont incompatible ou encore qu'il en manque tout simplement. C'est vrai pour la quasi totalité des projets dans l'industrie et c'est donc ok si le cahier des charges n'est pas parfait du premier coup.

## Organisation du projet

C'est maintenant que nous allons découper notre projet en tâches en fonction des spécifications que nous avons listées.
Pour être efficace, une tâche doit être réalisable en 1 à 2 jours. Si ça n'est pas le cas, c'est qu'il faut certainement découper cette tâche. Cela permet de savoir ou donner de la tête, de mieux estimer la durée du projet et surtout, ça nous rassure quant à la faisabilité du projet dans sa globalité.
Voici donc les tâches que j'ai retenues :

1. Comprendre le fonctionnement d'un drone

2. Faire un état des lieux du drone dont nous disposons (liste des composants, câblage, documentations)

3. Définir une architecture logicielle

4. Créer l'arborescence du projet

5. Créer une structure pour les tests unitaires

6. Developer une CLI (Command Line Interface) pour simplifier les futurs besoins en débogage

7. Developer le driver pour l'accéléromètre

8. Developer le driver pour le gyroscope

9. Developer le driver pour le pilotage des moteurs

10. Developer le driver pour le récepteur radio

11. Developer l'algorithme de PID (Proportionnel, Intégral, Dérivé)

12. Developer les tâches d'acquisitions des données

13. Developer la tâche de la gestion des moteurs en fonctions des données capteurs + radio

J'écrirai un article par tâche (ou par deux tâches en fonction de la complexité). Donc à coup d'une tâche par semaine, le projet devrait nous prendre environ 3 mois.

Vous pourrez suivre l'avancé du projet grâce à mon [Github](https://github.com/lchagnoleau). La première phase du projet, à savoir, comprendre le fonctionnement d'un drone commencera la semaine prochaine.