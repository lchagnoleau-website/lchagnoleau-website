---
title: "FreeRTOS from scratch sur STM32F446 Partie 2/2"
date: 2021-01-31T12:15:00+01:00
draft: false
---


On s'attaque aujourd'hui à la deuxième partie de l'article à savoir, l'intégration dans notre projet d'un système d'exploitation temps réel : FreeRTOS.

# Qu'est ce qu'un OS temps réel ?

Dans les systèmes embarqués, il y a des cas ou l'on est contraint de respecter des temps d’exécutions donnés. Pennons l'exemple du système Airbag dans l'automobile. Il est primordial lors d'un choc frontal que l'Airbag se déclenche en _x_ ms. ni plus, ni moins. Il est donc important de garantir un délai fixe entre le déclencheur et l'action. Comment faire cela avec un processeur qui gère déjà une multitude de capteurs et fonctionnalités ? Grâce un OS temps réel.  
Un RTOS (_Real Time Operating System_) se défini donc comme suit :

-   Il est capable gérer le temps de manière **précise et répétable**. Par exemple, on souhaite qu'une action se déclenche 1 seconde après un événement plus ou moins 1ms. Un RTOS permettra se genre de comportement à l'inverse d'un OS non temps réel comme Windows ou autre.
-   Pour ce faire, l'OS temps réel fonctionne par **tâche dont il gère les priorités**. C'est ce point là qui est important car, Windows aussi gère des tâches, mais il ne gère pas les priorités.

Maintenant que l'on a vu rapidement ce qu'est un OS temps réel, passons à la pratique

# Installation de FreeRTOS

RDV sur la page de téléchargement de [FreeRTOS](https://www.freertos.org/a00104.html).  
Ensuite, on crée un répertoire _Third_Party/FreeRTOS_ à la racine de notre projet. C'est ici que nous allons copier les sources de FreeRTOS.

On copie donc tout ce qui se trouve dans _FreeRTOSv202012.00/FreeRTOS/Source_ de ce que l'on vient de télécharger dans _Third_Party/FreeRTOS_ fraîchement créé.

Il faut par contre faire un peu de ménage car nous n'avons pas besoin de toutes les sources.

-   Dans _Third_Party/FreeRTOS/portable_ : On supprime tout **sauf** _GCC_/ et _MemMang_/
-   Dans _Third_Party/FreeRTOS/portable/GCC_ : On supprime tout **sauf** _ARM_CM4F_/ (si comme moi vous utilisez un Cortex-M4)
-   Dans _Third_Party/FreeRTOS/portable/Memang_ : On supprime tout **sauf** _heap_4.c_

Il nous faut également un exemple de fichier de configuration. On peut le récupérer ici :  
_FreeRTOSv202012.00/FreeRTOS/Demo/CORTEX_M4F_STM32F407ZG-SK/FreeRTOSConfig.h_  
Et on le place dans _app/inc_.

# Intégration au projet

## FreeRTOSConfig.h

Il nous faut légèrement modifier le fichier de configuration de FreeRTOS afin de l'adapter à notre projet :

```c
/*
 * FreeRTOS V202012.00
 * Copyright (C) 2020 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */


#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE.
 *
 * See http://www.freertos.org/a00110.html
 *----------------------------------------------------------*/

/* Ensure stdint is only used by the compiler, and not the assembler. */
#include "stm32f4xx.h"

#define configUSE_PREEMPTION			1
#define configUSE_IDLE_HOOK				0
#define configUSE_TICK_HOOK				0
#define configCPU_CLOCK_HZ				( SystemCoreClock )
#define configTICK_RATE_HZ				( ( TickType_t ) 1000 )
#define configMAX_PRIORITIES			( 5 )
#define configMINIMAL_STACK_SIZE		( ( unsigned short ) 130 )
#define configTOTAL_HEAP_SIZE			( ( size_t ) ( 75 * 1024 ) )
#define configMAX_TASK_NAME_LEN			( 10 )
#define configUSE_TRACE_FACILITY		1
#define configUSE_16_BIT_TICKS			0
#define configIDLE_SHOULD_YIELD			1
#define configUSE_MUTEXES				1
#define configQUEUE_REGISTRY_SIZE		8
#define configCHECK_FOR_STACK_OVERFLOW	0
#define configUSE_RECURSIVE_MUTEXES		1
#define configUSE_MALLOC_FAILED_HOOK	0
#define configUSE_APPLICATION_TASK_TAG	0
#define configUSE_COUNTING_SEMAPHORES	1
#define configGENERATE_RUN_TIME_STATS	0

/* Co-routine definitions. */
#define configUSE_CO_ROUTINES 		0
#define configMAX_CO_ROUTINE_PRIORITIES ( 2 )

/* Software timer definitions. */
#define configUSE_TIMERS				1
#define configTIMER_TASK_PRIORITY		( 2 )
#define configTIMER_QUEUE_LENGTH		10
#define configTIMER_TASK_STACK_DEPTH	( configMINIMAL_STACK_SIZE * 2 )

/* Set the following definitions to 1 to include the API function, or zero
to exclude the API function. */
#define INCLUDE_vTaskPrioritySet		1
#define INCLUDE_uxTaskPriorityGet		1
#define INCLUDE_vTaskDelete				1
#define INCLUDE_vTaskCleanUpResources	1
#define INCLUDE_vTaskSuspend			1
#define INCLUDE_vTaskDelayUntil			1
#define INCLUDE_vTaskDelay				1

/* Cortex-M specific definitions. */
#ifdef __NVIC_PRIO_BITS
	/* __BVIC_PRIO_BITS will be specified when CMSIS is being used. */
	#define configPRIO_BITS       		__NVIC_PRIO_BITS
#else
	#define configPRIO_BITS       		4        /* 15 priority levels */
#endif

/* The lowest interrupt priority that can be used in a call to a "set priority"
function. */
#define configLIBRARY_LOWEST_INTERRUPT_PRIORITY			0xf

/* The highest interrupt priority that can be used by any interrupt service
routine that makes calls to interrupt safe FreeRTOS API functions.  DO NOT CALL
INTERRUPT SAFE FREERTOS API FUNCTIONS FROM ANY INTERRUPT THAT HAS A HIGHER
PRIORITY THAN THIS! (higher priorities are lower numeric values. */
#define configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY	5

/* Interrupt priorities used by the kernel port layer itself.  These are generic
to all Cortex-M ports, and do not rely on any particular library functions. */
#define configKERNEL_INTERRUPT_PRIORITY 		( configLIBRARY_LOWEST_INTERRUPT_PRIORITY << (8 - configPRIO_BITS) )
/* !!!! configMAX_SYSCALL_INTERRUPT_PRIORITY must not be set to zero !!!!
See http://www.FreeRTOS.org/RTOS-Cortex-M3-M4.html. */
#define configMAX_SYSCALL_INTERRUPT_PRIORITY 	( configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY << (8 - configPRIO_BITS) )
	
/* Normal assert() semantics without relying on the provision of an assert.h
header file. */
#define configASSERT( x ) if( ( x ) == 0 ) { taskDISABLE_INTERRUPTS(); for( ;; ); }	
	
/* Definitions that map the FreeRTOS port interrupt handlers to their CMSIS
standard names. */
#define vPortSVCHandler SVC_Handler
#define xPortPendSVHandler PendSV_Handler
#define xPortSysTickHandler SysTick_Handler

#endif /* FREERTOS_CONFIG_H */


```

-   ligne 45 : On inclut le fichier "_stm32f4xx.h_"
-   ligne 48 et 49 : On désactive _configUSE_IDLE_HOOK_ et _configUSE_TICK_HOOK_. Il s'agit de fonction de callbacks à implémenter afin de gérer certaines fonctionnalités de FreeRTOS. Ici, on veut faire quelque chose de simple donc pas besoin.
-   ligne 61 et 63 : Idem pour _configCHECK_FOR_STACK_OVERFLOW_ et _configUSE_MALLOC_FAILED_HOOK_

## main.c

Il y a des choses à dire ici. Dans l'ordre des modifications :

1.  On inclut les fichiers _FreeRTOS.h_ et _task.h_
2.  FreeRTOS fonctionne par tâche. Et chaque tâche créée à besoin d'un TCB (_task control block_). Le TCB contient les différentes informations nécessaire à chaque tâche.
3.  Concrètement, chaque tâche sera définit par une fonction. On crée donc le prototype de cette fonction et la fonction en elle même. Dans notre exemple, on fait simplement bagoter la led chaque seconde.
4.  Dans la fonction main(), On crée notre tache grâce à la fonction xTaskCreate(). Cette fonction prend en argument : un pointeur vers la fonction de notre tâche (défini au point 3), le nom de la tâche, la taille de la stack à allouer, d'éventuels paramètres, la priorité de la tâche (0 = priorité la plus faible) et le handler de la tâche (défini au point 2).
5.  On lance le scheduler.

```c
#include <stdlib.h>
#include "stm32f4xx.h"
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"

TaskHandle_t xTaskLedHandle1 = NULL;

void vTask_Led_handler_1(void *params);

int main(void)
{
    /* Hardware board init */
    board_hardware_init();

    /* Create task */
    xTaskCreate(vTask_Led_handler_1, "Task-1", 500, NULL, 1, &xTaskLedHandle1);

    /* Start scheduler */
    vTaskStartScheduler();

    while(1)
    {
        /* Never go here */
    }

    return 0;
}

void vTask_Led_handler_1(void *params)
{
	while(1)
	{
        toggle_led();
        vTaskDelay(pdMS_TO_TICKS(1000));
	}
}
```

## makefile

Puisque l'on a ajouter des fichiers à notre projet, il faut adapter notre makefile en y rajoutant simplement les dossier de FreeRTOS :

```makefile
BINDIR = bin
OBJDIR = obj
OUTPUT = STM32F446_VS_CODE_TEMPLATE
RELEASEDIR = release
CC = arm-none-eabi-gcc
RM = rm -rf
MK = mkdir -p
PWD = $(shell pwd)

CC_INCLUDE = 	app/inc \
				drivers/st/inc \
				CMSIS/inc \
				CMSIS/device/inc \
				Third_Party/FreeRTOS/include \
				Third_Party/FreeRTOS/portable/GCC/ARM_CM4F \

CC_SOURCE = 	app/src \
				drivers/st/src \
				CMSIS/device/src \
				Third_Party/FreeRTOS \
				Third_Party/FreeRTOS/portable/GCC/ARM_CM4F \
				Third_Party/FreeRTOS/portable/MemMang \

S_SOURCE = 		startup

CC_SOURCES := 	$(shell find $(CC_SOURCE) -maxdepth 1 -name '*.c')
S_SOURCES := 	$(shell find $(S_SOURCE) -maxdepth 1 -name '*.s')

OBJECTS :=		$(addprefix $(OBJDIR)/, $(CC_SOURCES:.c=.o) $(S_SOURCES:.s=.o) $(STA_SOURCES:.S=.o))

OBJDIRS := 		$(patsubst %, $(OBJDIR)/%, $(CC_SOURCE)) \
				$(patsubst %, $(OBJDIR)/%, $(S_SOURCE))

VPATH += $(CC_SOURCE) $(S_SOURCES) $(STA_SOURCES)

CC_FLAGS = -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -specs=rdimon.specs -lc -lrdimon
CC_FLAGS_OBJS = -DUSE_STDPERIPH_DRIVER -DSTM32F446xx
CC_FLAGS_ELF = $(CC_FLAGS) -T"linker.ld" -Wl,-Map=bin/output.map -Wl,--gc-sections
CC_PARAMS=$(foreach d, $(CC_INCLUDE), -I$d)

all: $(OUTPUT).elf

$(OUTPUT).elf: dir $(OBJDIRS) $(OBJECTS) linker.ld
	@echo 'Building target: $@'
	@echo 'Invoking: MCU GCC Linker'
	$(CC) $(CC_FLAGS_ELF) -o $(BINDIR)/$(OUTPUT).elf $(OBJECTS) -lm
	@echo 'Finished building target: $@'
	@echo ' '
	$(MAKE) --no-print-directory post-build

$(OBJDIR)/%.o: %.s
	@echo 'Building file: $@'
	@echo 'Invoking: C Compiler'
	$(CC) $(CC_FLAGS) $(CC_FLAGS_OBJS) $(CC_PARAMS) -DDEBUG -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o $@ $<
	@echo 'Finished building: $@'
	@echo ' '

$(OBJDIR)/%.o: %.S
	@echo 'Building file: $@'
	@echo 'Invoking: C Compiler'
	$(CC) $(CC_FLAGS) $(CC_FLAGS_OBJS) $(CC_PARAMS) -DDEBUG -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o $@ $<
	@echo 'Finished building: $@'
	@echo ' '

$(OBJDIR)/%.o: %.c
	@echo 'Building file: $@'
	@echo 'Invoking: C Compiler'
	$(CC) $(CC_FLAGS) $(CC_FLAGS_OBJS) $(CC_PARAMS) -DDEBUG -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o $@ $<
	@echo 'Finished building: $@'
	@echo ' '

$(OBJDIRS):
	mkdir -p $@ 

.PHONY: post-build dir clean
post-build:
	-@echo 'Generating binary and Printing size information:'
	arm-none-eabi-objcopy -O binary "$(BINDIR)/$(OUTPUT).elf" "$(BINDIR)/$(OUTPUT).bin"
	arm-none-eabi-size "$(BINDIR)/$(OUTPUT).elf"
	-@echo ' '

dir:
	@echo 'Creat output folders'
	@echo $(OBJECTS)
	$(MK) $(BINDIR) $(OBJDIR)
	@echo ' '

clean:
	@echo 'Clean output folders'
	$(RM) $(BINDIR)/ $(OBJDIR)/ $(RELEASEDIR)/
	@echo ' '
```

## Compilation

On lance la commande _make_ et si tout se passe bien, notre binaire devrait sa générer. On la flash dans la carte et c'est fini.

# Conclusion

Ceci était un premier pas dans le monde de FreeRTOS. Nous n'avons vu là qu'environ 1% du potentiel de ce dernier mais nous le verrons plus en profondeur lors des prochains articles dans lesquels j’intégrerai l'OS dans des cas plus concrets.

N'oubliez pas que l'ensemble des sources du projet est disponible sur [GitHub](https://github.com/lchagnoleau/STM32_Template).