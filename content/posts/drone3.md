---
title: "Drone software from scratch 3/x"
date: 2021-02-20T15:00:00+01:00
draft: false
---

## L'architecture logicielle

L'étape d'aujourd'hui consiste à définir l'architecture logicielle du projet. Elle sert de base au futur code que nous allons developer.
C'est donc un travail important qui aidera grandement lors du development du logiciel embarqué.

Pour cela, nous allons utiliser deux types de diagrammes :
- [Diagramme de classe](https://fr.wikipedia.org/wiki/Diagramme_de_classes)
- [Diagramme de séquence](https://fr.wikipedia.org/wiki/Diagramme_de_s%C3%A9quence)

## Architecture générale

J'ai utilisé un diagramme de classe pour créer mon architecture générale :

{{< image src="/drone3/software_architecture.png" alt="Architecture générale" position="center" style="border-radius: 8px;" >}}

Commençons du bas vers le haut :
1. En rose nous retrouvons nos différents périphériques physiques
2. J'y ai annoté en jaune les connexions aux micro-contrôleur.
3. En vert nous avons les classes drivers qui permettent de contrôler nos périphériques. Avec en vert clair, chaque fonctions qu'il faudra developer.
4. En bleu foncé, ce sont les tâches avec en jaune leur niveau de priorité (plus le chiffre est grand, plus elle est prioritaire)
5. Enfin, en bleu clair, nous avons les Queues utilisée permettant de d'envoyer des données entre les tâches.

Notez que, tout comme les prochains diagrammes que nous verrons, il existe bien entendu une multitude de façon d'architecturer un logiciel et j'en ai d'ailleurs fait plusieurs autres avant de m'arrêter sur celle-ci.
Normalement, on sait que l'architecture que l'on vient de créer est correcte lorsque elle est simple à comprendre.

## Diagrammes de séquences

Les diagrammes de séquence permettent de schématiser les appels de fonctions entre les différents modules du logicielle.

### Tâche 'Sensors'

{{< image src="/drone3/msc_sensors_task.png" alt="MSC Sensors" position="center" style="border-radius: 8px;" >}}

1. On initialise la classe **icm20602**
2. On entre dans une boucle infinie
3. On récupère les mesures de l'accéléromètre et du gyroscope
4. On envoi ces mesures dans la Queue 'xQueueSensors'
5. On attends x nano-secondes
6. On repart à l'étape 3

### Tâche 'Receiver'

{{< image src="/drone3/msc_receiver_task.png" alt="MSC Receiver" position="center" style="border-radius: 8px;" >}}

1. On initialise la classe **x4r**
2. On entre dans une boucle infinie
3. On attend une interruption (qui viendra de l'UART)
4. On parse les données reçues par le périphérique
5. On envoi ces données dans la Queue 'xQueueReceiver'
6. On repart à l'étape 3

### Tâche 'PID'

{{< image src="/drone3/msc_pid_task.png" alt="MSC PID" position="center" style="border-radius: 8px;" >}}

1. On initialise la classe **rs20a**
2. On entre dans une boucle infini
3. On attend que la Queue 'xQueueSensors' ne soit plus vide
4. On récupère les données de la Queue 'xQueueSensors'
5. On récupère si disponible les données de la Queue 'xQueueReceiver'
6. On récupère les consignes actuelles des moteurs
7. On effectue le calcul pour les prochaines consignes moteurs en fonctions de tout ces éléments
8. On envoie ces nouvelles consignes aux moteurs
9. On repart à l'étape 3

## Conclusion sur l'architecture logicielle

L'architecture logicielle est un vrai métier qui peut nécessiter plusieurs jours/mois en fonction de la complexité du projet. Cependant, c'est une étape crucial qui permet de gagner un temps considerable lors du développement.

En effet, cela donne une vision global du fonctionnement du logiciel et de ce fait, de nombreux bugs potentiels peuvent être identifiés avant même d'avoir écrit la première ligne de code.

Enfin, cette architecture va définir l'arborescence de notre projet et guider le développement.
