---
title: "FreeRTOS from scratch sur STM32F446 Partie 1/2"
date: 2021-01-23T19:18:00+01:00
draft: false
---

## Introduction

Je vais vous montrer au travers de ces 2 articles comment créer un projet FreeRTOS from scratch et comment le flasher dans une Nucleo. On y verra notamment quels outils/logiciels sont nécessaire, comment démarrer simplement sur Nucleo, utiliser les outils de débogages et pour finir, comment intégrer FreeRTOS à nos projets et à quoi ça sert.

Dans le premier article, on créera notre projet from scratch. Ce sera dans le second article que nous y intégrerons FreeRTOS.

# Liens et préparation

Personnellement, j'utilise [Visual Studio Code](https://code.visualstudio.com/) qui est un IDE modern, puissant, léger et très agréable.  
Vous aurez également besoin des extensions suivantes : [Cortex-Debug](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug), [C++ Intellisense](https://marketplace.visualstudio.com/items?itemName=austin.code-gnu-global), [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools).

Concernant la carte Nucleo que j'utilise, il s'agit de la STM32F446RE. Tous les documents que j'utilise à sont sujet sont disponible sur le site de ST : [STM32F446RE](https://www.st.com/en/microcontrollers-microprocessors/stm32f446re.html) et la [Nucleo](https://www.st.com/en/evaluation-tools/nucleo-f446re.html).

Il vous faudra également installer les différents paquets nécessaire à la compilation :

```bash
sudo apt-get -y install gcc-arm-none-eabi binutils-arm-none-eabi build-essential make
```

Enfin, les sources du projet sont sur mon [GitHub](https://github.com/lchagnoleau/STM32_Template).

# Création du projet et premier code

## Arborescence du projet

J'ai l’habitude d'organiser mes projets comme suit :

-   app
    -   src
    -   inc
-   CMSIS
-   drivers
-   startup
-   Third_Party
    -   FreeRTOS

Le dossier _app/_ contiendra nos sources d'application (main, config...).  
Le dossier _CMSIS/_ contiendra les sources permettant l'interaction avec le Cortex-ARM.  
Le dossier _drivers/_ contiendra les drivers fourni par ST.  
Le dossier _startup/_ contiendra le startup file.  
Le dossier _Third_Party/_ contiendra les sources FreeRTOS.

## Récupération drivers

Commençons donc par récupérer les drivers fourni par ST pour notre Nucleo. Nous aurons besoins des fichiers CMSIS, startup et des drivers bas-niveau.

Tous cela ce trouve sur le site de ST, à cette [adresse](https://my.st.com/content/my_st_com/en/products/embedded-software/mcu-mpu-embedded-software/stm32-embedded-software/stm32-standard-peripheral-libraries/stsw-stm32065.license=1611380359274.product=STSW-STM32065.version=1.8.0.html). Vous allez voir, cette archive contient beaucoup de chose, mais nous ne nous servirons pas de tout.

C'est parti ! On crée un joli répertoire sur notre ordinateur puis on crée les dossiers suivants:

![](/freertos/arbo_void-2.png)

On copie :

-   Tous les includes depuis _STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Include_ vers _CMSIS/inc_
-   Tous les includes depuis _STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Device/ST/STM32F4xx/Include_ vers _CMSIS/device/inc_
-   le fichier _system_stm32f4xx.c_ depuis _STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Device/ST/STM32F4xx/Source/Templates_ vers _CMSIS/device/src_
-   Tous les includes depuis _STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/STM32F4xx_StdPeriph_Driver/inc_ vers _drivers/st/inc_
-   Toutes les sources depuis _STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/STM32F4xx_StdPeriph_Driver/src_ vers _drivers/st/_src
-   Le fichier _startup_stm32f446xx.s_ depuis _STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Device/ST/STM32F4xx/Source/Templates/SW4STM32_ vers _startup/_
-   Le fichier _stm32f4xx_conf.h_ depuis _STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Project/STM32F4xx_StdPeriph_Templates_ vers _app/inc_
-   Le fichier STM32F446ZETx_FLASH.ld depuis _STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Project/STM32F4xx_StdPeriph_Templates/SW4STM32/STM32F446xx_ vers le repertoire racine. On le renommera en _linker.ld_

Une dernière chose. Pensez à supprimer les fichiers _stm32f4xx_fsmc.c_ et _stm32f4xx_fsmc.h_ des répertoires _drivers/st/src_ et _drivers/st/inc_. Nous n'en aurons pas besoin et cela créera des conflits.

## Application

Bien, comme dirai l'autre, ça c'est fait ! On va maintenant coder notre application.  
Pour cela, créons 3 fichiers :

-   _app/src/main.c_ : il contiendra notre main
-   _app/src/board.c_ : nos différentes fonctions d'initialisations
-   _app/inc/board.h_ : les prototypes de board.c

### board.h

Très simplement, nous allons créer les prototypes pour les fonctions d'initialisation et d’interaction avec la led et le bouton poussoir :

```c
#ifndef __BOARD_H
#define __BOARD_H

#include <stdbool.h>


void board_hardware_init();
bool is_button_pressed();
void toggle_led();

#endif
```

## board.c

Un peu plus gros mais pas plus compliqué, je vous le met ci dessous et j'y reviens fonction par fonction juste après :

```c
#include "board.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"

static void led_init();
static void button_init();

void board_hardware_init()
{
    /* Set default RCC clock -> 16MHz */
    RCC_DeInit();
    SystemCoreClockUpdate();

    /* Init Led */
    led_init();

    /* Init button */
    button_init();
}

static void led_init()
{
    /* GREEN Led to PA5
    * 
    * the I/O is HIGH, the LED is on
    * the I/O is LOW,  the LED is off
    *
    */

    /* Init struct */
    GPIO_InitTypeDef gpio_init_struct;

    memset(&gpio_init_struct, 0, sizeof(gpio_init_struct));

    GPIO_StructInit(&gpio_init_struct);

    /* Start clock APB1 (GPIOA) */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    /* Init GPIO PA5 */
    gpio_init_struct.GPIO_Pin = GPIO_Pin_5;
    gpio_init_struct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_Init(GPIOA, &gpio_init_struct);
}

static void button_init()
{
    /* B1 to PC13 */

    /* Init struct */
    GPIO_InitTypeDef gpio_init_struct;

    memset(&gpio_init_struct, 0, sizeof(gpio_init_struct));

    GPIO_StructInit(&gpio_init_struct);

    /* Start clock APB1 (GPIOC) */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

    /* Init GPIO PA5 */
    gpio_init_struct.GPIO_Pin = GPIO_Pin_13;
    gpio_init_struct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(GPIOC, &gpio_init_struct);
}

bool is_button_pressed()
{
    return (bool) !GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13);
}

void toggle_led()
{
    GPIO_ToggleBits(GPIOA, GPIO_Pin_5);
}
```

Dans la fonction _board_hardware_init()_, nous configurons le RCC (Reset and Clock Control )par défaut. Le RCC étant le registre pilotant les horloge du processeur. Si nous faisons un CTRL+Click sur la fonction _RCC_DeInit();_ nous arrivons sur la déclaration de la fonction qui nous indique :

```c
/**
  * @brief  Resets the RCC clock configuration to the default reset state.
  * @note   The default reset state of the clock configuration is given below:
  *            - HSI ON and used as system clock source
  *            - HSE, PLL and PLLI2S OFF
  *            - AHB, APB1 and APB2 prescaler set to 1.
  *            - CSS, MCO1 and MCO2 OFF
  *            - All interrupts disabled
  * @note   This function doesn't modify the configuration of the
  *            - Peripheral clocks  
  *            - LSI, LSE and RTC clocks 
  * @param  None
  * @retval None
  */
```

Si on fait l'effort d'aller voir dans la datasheet, on s’aperçoit que le micro sera donc cadencé à 16 Mhz.  
_SystemCoreClockUpdate();_ permet de mettre à jour la variable indiquant la fréquence de l'horloge.

Dans la fonction _static void led_init()_, on configure la GPIO PA5, qui correspond sur la Nucleo à la led en sortie. la ligne _RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);_ sert à activer le bus AHB1 sur lequel est branché le GPIO Port A. On le sait grâce à la doc :

![](/freertos/doc_st_horloge.png)

La fonction _static void button_init()_ est similaire à la fonction _led_init()_.

Enfin, les fonctions _bool is_button_pressed()_ et _void toggle_led()_ utilise la librairie fourni par ST.

## main.c

Pour finir, le fichier _main.c_ :

```c
#include <stdlib.h>
#include "stm32f4xx.h"
#include "board.h"


int main(void)
{
    /* Hardware board init */
    board_hardware_init();

    while(1)
    {
        if(is_button_pressed())
            toggle_led();
    }

    return 0;
}
```

Nous initialisons la carte avec la fonction précédemment vu et dans la boucle infini et afin de tester que tout va bien, on fait simplement bagoter la led dès qu'on appui sur le bouton poussoir.  
Reste plus qu'à compiler tout ça !

# Compilation

Pour compiler toutes nos source, on va se créer un petit makefile.

On crée donc un fichier _makefile_ à la racine de notre projet :

```makefile
BINDIR = bin
OBJDIR = obj
OUTPUT = STM32F446_VS_CODE_TEMPLATE
RELEASEDIR = release
CC = arm-none-eabi-gcc
RM = rm -rf
MK = mkdir -p
PWD = $(shell pwd)

CC_INCLUDE = 	app/inc \
				drivers/st/inc \
				CMSIS/inc \
				CMSIS/device/inc

CC_SOURCE = 	app/src \
				drivers/st/src \
				CMSIS/device/src

S_SOURCE = 		startup

CC_SOURCES := 	$(shell find $(CC_SOURCE) -maxdepth 1 -name '*.c')
S_SOURCES := 	$(shell find $(S_SOURCE) -maxdepth 1 -name '*.s')

OBJECTS :=		$(addprefix $(OBJDIR)/, $(CC_SOURCES:.c=.o) $(S_SOURCES:.s=.o) $(STA_SOURCES:.S=.o))

OBJDIRS := 		$(patsubst %, $(OBJDIR)/%, $(CC_SOURCE)) \
				$(patsubst %, $(OBJDIR)/%, $(S_SOURCE))

VPATH += $(CC_SOURCE) $(S_SOURCES) $(STA_SOURCES)

CC_FLAGS = -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -specs=rdimon.specs -lc -lrdimon
CC_FLAGS_OBJS = -DUSE_STDPERIPH_DRIVER -DSTM32F446xx
CC_FLAGS_ELF = $(CC_FLAGS) -T"linker.ld" -Wl,-Map=bin/output.map -Wl,--gc-sections
CC_PARAMS=$(foreach d, $(CC_INCLUDE), -I$d)

all: $(OUTPUT).elf

$(OUTPUT).elf: dir $(OBJDIRS) $(OBJECTS) linker.ld
	@echo 'Building target: $@'
	@echo 'Invoking: MCU GCC Linker'
	$(CC) $(CC_FLAGS_ELF) -o $(BINDIR)/$(OUTPUT).elf $(OBJECTS) -lm
	@echo 'Finished building target: $@'
	@echo ' '
	$(MAKE) --no-print-directory post-build

$(OBJDIR)/%.o: %.s
	@echo 'Building file: $@'
	@echo 'Invoking: C Compiler'
	$(CC) $(CC_FLAGS) $(CC_FLAGS_OBJS) $(CC_PARAMS) -DDEBUG -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o $@ $<
	@echo 'Finished building: $@'
	@echo ' '

$(OBJDIR)/%.o: %.S
	@echo 'Building file: $@'
	@echo 'Invoking: C Compiler'
	$(CC) $(CC_FLAGS) $(CC_FLAGS_OBJS) $(CC_PARAMS) -DDEBUG -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o $@ $<
	@echo 'Finished building: $@'
	@echo ' '

$(OBJDIR)/%.o: %.c
	@echo 'Building file: $@'
	@echo 'Invoking: C Compiler'
	$(CC) $(CC_FLAGS) $(CC_FLAGS_OBJS) $(CC_PARAMS) -DDEBUG -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o $@ $<
	@echo 'Finished building: $@'
	@echo ' '

$(OBJDIRS):
	mkdir -p $@ 

.PHONY: post-build dir clean
post-build:
	-@echo 'Generating binary and Printing size information:'
	arm-none-eabi-objcopy -O binary "$(BINDIR)/$(OUTPUT).elf" "$(BINDIR)/$(OUTPUT).bin"
	arm-none-eabi-size "$(BINDIR)/$(OUTPUT).elf"
	-@echo ' '

dir:
	@echo 'Creat output folders'
	@echo $(OBJECTS)
	$(MK) $(BINDIR) $(OBJDIR)
	@echo ' '

clean:
	@echo 'Clean output folders'
	$(RM) $(BINDIR)/ $(OBJDIR)/ $(RELEASEDIR)/
	@echo ' '
```

C'est un _makefile_ assez classique et surtout très générique que vous pourrez réutiliser dans vos futurs projets. Je ne le détaille pas car cela serai un poil compliqué mais si vous le desirez, je peux en faire un prochain article.

Maintenant, il suffit simplement de lancer la commande make pour lancer la compilation et générer notre binaire :

```bash
make
```

A la fin, si tout ce passe bien, vous obtiendrez :

```bash
Generating binary and Printing size information:
arm-none-eabi-objcopy -O binary "bin/STM32F446_VS_CODE_TEMPLATE.elf" "bin/STM32F446_VS_CODE_TEMPLATE.bin"
arm-none-eabi-size "bin/STM32F446_VS_CODE_TEMPLATE.elf"
   text    data     bss     dec     hex filename
   2744    1104    1604    5452    154c bin/STM32F446_VS_CODE_TEMPLATE.elf
```

Signe que la compilation s'est correctement terminée.

nous allons maintenant flasher la carte.

# Test sur Nucleo

## Configuration des outils de débogages

Si vous utilisez une carte Nucleo comme moi, sachez que la partie supérieur de la carte est en fait un flasher/débogueur du nom de ST-LINKV2. Personnellement, cela fait plusieurs année que je convertie systématiquement mes carte STLINKV2 en flasher J-Link. Je trouve que grâce à cela, les performances en débogage sont accrue.

Pour ce faire, il faut passer sur Windows très brièvement et télécharger l'outils fournie par J-Link à cette [adresse](https://www.segger.com/products/debug-probes/j-link/models/other-j-links/st-link-on-board/). On suit les instructions fourni par SEGGER et on se retrouve avec une superbe carte J-Link. On peut du coup repasser sur Linux.

On va maintenant configurer Studio Code. Dans la partie "débogueur" à gauche, on click sur "create a launch.json file", puis on selectionne "Cortex Debug".

![](/freertos/debog.png)

Ainsi, il nous crée donc un fichier _.vscode/launch.json_  
On le modifie légèrement pour l'adapter à notre projet :

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Cortex Debug",
            "cwd": "${workspaceRoot}",
            "executable": "./bin/STM32F446_VS_CODE_TEMPLATE.elf",
            "request": "launch",
            "type": "cortex-debug",
            "servertype": "jlink",
            "device": "STM32F446RE",
            "interface": "swd",
            "svdFile": ".vscode/STM32F446.svd",
            "runToMain": true,
        }
    ]
}
```

Remarquez que j'ai ajouté une ligne _"svdFile": ".vscode/STM32F446.svd"_. Un fichier svd contient les adresses des différents périphériques du microcontrôleur. On pourra donc lors du débogage parcourir les périphériques à la volée. Ce n'est pas obligatoire mais vous pouvez récupérer ce fichier svd [ici](https://github.com/posborne/cmsis-svd/blob/master/data/STMicro/STM32F446.svd) et le placer dans le dossier _.vscode_

## Flash

On a presque fini. Ne reste plus qu'à envoyer notre binaire sur la cible. Il suffit d'appuyer sur la touche F5. Le programme est ensuite téléversé sur dans la flash du microcontrôleur.

L'outils de débogage se lance alors :

![](/freertos/arbo.png)

Le programme s'est gentiment arrêté à la première instruction de notre main. On peut lancer le code, mettre pause... grâce aux boutons au dessus. Très utile aussi, nous avons la possibilité de mettre en place des _breakpoints_ comme je l'ai fait ici à la ligne 14. Il suffit juste de cliquer à côté du chiffre de la ligne en question. Ainsi, le code se mettra en pause dès qu'il atteindra cette ligne. Dans le cas présent, dès que j'appuierai sur le bouton poussoir.

# Conclusion

Voilà, nous avons vu comment créer un projet from scratch pour jouer avec notre Nucleo. Je sais qu'il existe pléthore d'IDE permettant de générer du code et ce genre de projet automatiquement. L'avantage avec la méthode que je vous ai expliquée, c'est que l'on comprends beaucoup mieux ce que l'on fait. On peut aussi très facilement utiliser l'IDE que l'on veut et surtout, il n'y a aucun fichier superflue. N'hésitez pas à me dire ce que vous en pensez en commentaire. La semaine prochaine ferai une deuxième partie ou l'on intégrera le noyaux FreeRTOS !  
N'oubliez pas que vous pouvez retrouver les sources du projet sur [GitHub](https://github.com/lchagnoleau/STM32_Template).