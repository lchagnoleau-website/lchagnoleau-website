---
title: "Drone software from scratch 4/x"
date: 2021-03-06T12:00:00+01:00
draft: false
asciinema: true
---

Aujourd'hui nous allons créer l'arborescence du projet. Je vous invite à aller voir mes deux articles qui explique comment créer un tel projet from scratch:
- [FreeRTOS from scratch sur STM32F446 Partie 1/2](https://loic-chagnoleau.com/posts/freertos1/)
- [FreeRTOS from scratch sur STM32F446 Partie 2/2](https://loic-chagnoleau.com/posts/freertos2/)

Je passerai donc rapidement sur cette partie.

## Installation des outils

On va commencer par installer les outils dont nous auront besoin pour compiler et flasher le binaire :

```bash
sudo apt install dfu-util
sudo apt install cmake
sudo apt install binutils-arm-none-eabi gcc-arm-none-eabi
```

- **arm-none-eabi** sont les outils de cross-compilation pour architecture ARM
- **cmake** permet de simplifier la compilation
- **dfu-utils** permet de flasher le binaire dans la flash en utilisant le mode DFU

## Test avant de commencer

Avant de commencer, il est bon de tester que les outils fonctionnent.
Pour cela, je vais récupérer le binaire du projet [Betaflight](https://github.com/betaflight/betaflight/releases) et le flasher dans la Matek F405 dont je dispose.

P.S. Pour la demo, j'ai utilisé l'excellent [asciinema](https://asciinema.org/) qui permet de faire des screens du terminal en vidéo et en mode Ascii. Cela permet d'avoir des vidéos très légères mais surtout, vous **pouvez effectuer des copiers/coller directement dans la vidéo** !!

{{< asciinema key="001" rows="20" preload="1" >}}

Ainsi, nous sommes capable de correctement flasher un binaire dans le contrôleur de vol. Cependant, le debug sera très compliqué étant donné que les broches JTAG ne sont pas accessible...

## Création du projet

Comme je l'ai dit plus haut, je vais aller vite sur cette partie étant donné que j'ai déjà abordé la création de ce genre de projet [ici](https://loic-chagnoleau.com/posts/freertos1/).
Cependant, pour gagner du temps et aussi vous montrez une autres méthode pour créer un projet, j'ai utilisé le logiciel [AC6 system workbench](https://www.ac6-tools.com/content.php/content_sw4mcu/lang_fr_FR.xphp). Je ne l'utilise que pour créer l'arborescence du projet car il télécharge automatiquement les librairies ST dont le linker script et le startup file ce qui est très pratique.

Pour ce faire, une fois dans le logiciel, faite un "New" -> "C Project" -> "AC6 STM32 MCU Project" -> Next -> Next. De là on sélectionne l'onglet "MCU" et on cherche notre micro-contrôleur.
Ensuite, "Next", on sélectionne "Standard Peripheral Library (StdPeriph)", on clique sur "Download target firmware" et enfin sur "Finish".
Il ne reste plus qu'à récupérer les dossiers et les mettre dans notre projet.

J'ai ajouté en plus un fichier **software/app/inc/target.h** contenant tout les defines que nous auront besoin par la suite :

```c
#pragma once

#include "stm32f4xx.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

#define LED0_PIN                GPIO_Pin_9
#define LED0_PORT               GPIOB
#define LED0_RCC_Periph         RCC_AHB1Periph_GPIOB
#define LED1_PIN                GPIO_Pin_14
#define LED1_PORT               GPIOA
#define LED1_RCC_Periph         RCC_AHB1Periph_GPIOA

#define SPI1_SCK_PIN            GPIO_Pin_5
#define SPI1_MISO_PIN           GPIO_Pin_6
#define SPI1_MOSI_PIN           GPIO_Pin_7
#define SPI1_PORT               GPIOA

#define SPI1_CS_PIN             GPIO_Pin_2
#define SPI1_CS_PORT            GPIOC

#define UART2_RX_PIN            GPIO_Pin_3
#define UART2_TX_PIN            GPIO_Pin_2
#define UART2_PORT              GPIOA

#define MOTOR_S1_PIN            GPIO_Pin_6
#define MOTOR_S2_PIN            GPIO_Pin_7
#define MOTOR_S3_PIN            GPIO_Pin_8
#define MOTOR_S4_PIN            GPIO_Pin_9
#define MOTOR_PORT              GPIOC

#define MOTOR_S1_TIM            TIM3
#define MOTOR_S2_TIM            TIM8
#define MOTOR_S3_TIM            TIM8
#define MOTOR_S4_TIM            TIM8

#define MOTOR_S1_TIM_CHANNEL    TIM_Channel_1
#define MOTOR_S2_TIM_CHANNEL    TIM_Channel_2
#define MOTOR_S3_TIM_CHANNEL    TIM_Channel_3
#define MOTOR_S4_TIM_CHANNEL    TIM_Channel_4

```

Enfin, dans le main, je me suis pour l'instant contenté de faire clignoter la led :

```c
#include <stdlib.h>
#include <stdbool.h>
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"


TaskHandle_t xTaskLedHandle1 = NULL;

void vTask_Led_handler_1(void *params);

int main(void)
{
    /* Hardware board init */
    board_hardware_init();

    while(1)
    {
        /* Create task */
        xTaskCreate(
            vTask_Led_handler_1,
            "Task-LED-1",
            500,
            NULL,
            2,
            &xTaskLedHandle1);

        /* Start scheduler */
        vTaskStartScheduler();
    }

    return 0;
}

void vTask_Led_handler_1(void *params)
{
	while(1)
	{   
        toggle_led();
        vTaskDelay(pdMS_TO_TICKS(1000));
	}
}
```

## CMake

Autre nouveauté, j'ai décidé d'utiliser l'outils [CMake](https://cmake.org/) pour faciliter la compilation.
Jusqu'à présent je ne l'avais jamais réellement utilisé et j'ai donc suivi l'excellent tuto qui ce trouve [ici](https://dev.to/younup/cmake-on-stm32-the-beginning-3766). Il explique comment utiliser CMake dans un environnement STM32.

Je l'ai bien sûr adapté à mon projet ce qui donne le fichier **CMakeLists.txt** suivant :

```cmake
cmake_minimum_required(VERSION 3.16)

if(NOT DEFINED CMAKE_TOOLCHAIN_FILE)
    set(CMAKE_TOOLCHAIN_FILE "${CMAKE_SOURCE_DIR}/arm-none-eabi-gcc.cmake")
endif()

project(drone_software)

enable_language(C CXX ASM)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS ON)

aux_source_directory(software/app/src                                                   SOURCE)
aux_source_directory(software/StdPeriph_Driver/src                                      SOURCE)
aux_source_directory(software/Third_Party/FreeRTOS/org/Source                           SOURCE)
aux_source_directory(software/Third_Party/FreeRTOS/org/Source/portable/GCC/ARM_CM4F     SOURCE)
aux_source_directory(software/Third_Party/FreeRTOS/org/Source/portable/MemMang          SOURCE)

set(SOURCE "${SOURCE};software/startup/startup_stm32.s")

set(EXECUTABLE ${PROJECT_NAME}.elf)

add_executable(${EXECUTABLE} ${SOURCE})

target_compile_definitions(${EXECUTABLE} PRIVATE
        -DSTM32F40_41xxx
        -DUSE_STDPERIPH_DRIVER
        )

target_include_directories(${EXECUTABLE} PRIVATE
        software/app/inc
        software/CMSIS/core
        software/CMSIS/device
        software/StdPeriph_Driver/inc
        software/Third_Party/FreeRTOS/org/Source/include
        software/Third_Party/FreeRTOS/org/Source/portable/GCC/ARM_CM4F
        )

target_compile_options(${EXECUTABLE} PRIVATE
        -mcpu=cortex-m4
        -mthumb
        -mfloat-abi=hard
        -mfpu=fpv4-sp-d16
        -fdata-sections
        -ffunction-sections
        -Wall

        $<$<CONFIG:Debug>:-Og>
        $<$<CONFIG:Debug>:-g>

        $<$<CONFIG:Release>:-Os>
        $<$<CONFIG:Release>:-flto>

        $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>
        $<$<COMPILE_LANGUAGE:CXX>:-Wuseless-cast>
        $<$<COMPILE_LANGUAGE:CXX>:-Wsuggest-override>
        $<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>
        $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>
        $<$<COMPILE_LANGUAGE:CXX>:-D_Static_assert=static_assert>
        )
target_link_options(${EXECUTABLE} PRIVATE
        -T${CMAKE_SOURCE_DIR}/software/LinkerScript.ld
        -mcpu=cortex-m4
        -mthumb
        -mfloat-abi=hard
        -mfpu=fpv4-sp-d16
        -specs=nosys.specs
        -lc
        -lm
        -lnosys
        -Wl,-Map=${PROJECT_NAME}.map,--cref
        -Wl,--gc-sections,--print-memory-usage
        )

# Print executable size
add_custom_command(TARGET ${EXECUTABLE}
        POST_BUILD
        COMMAND arm-none-eabi-size ${EXECUTABLE})

# Create hex file
add_custom_command(TARGET ${EXECUTABLE}
        POST_BUILD
        COMMAND arm-none-eabi-objcopy -O ihex ${EXECUTABLE} ${PROJECT_NAME}.hex
        COMMAND arm-none-eabi-objcopy -O binary ${EXECUTABLE} ${PROJECT_NAME}.bin)

# Improve clean target
set_target_properties(${EXECUTABLE} PROPERTIES ADDITIONAL_CLEAN_FILES
        "${PROJECT_NAME}.bin;${PROJECT_NAME}.hex;${PROJECT_NAME}.map")
```

## Compilation

On crée un répertoire à la racine du projet qui contiendra tout nos fichiers compilés et les binaires :

```bash
mkdir build
cd build
```

On éxecute maintenant **CMake** en précisant que les fichiers de config se trouve au répertoire parent :

```bash
cmake ..
```

Si tout s'est bien passé, on désormais nos fichiers binaire et nous pouvons les flasher dans le contrôleur de vol :

```bash
dfu-util -a 0 -s 0x08000000:leave -D drone_software.bin -R
```

Vous ne pouvez pas le voir de vos yeux, mais chez moi, j'ai bien la led qui clignote 1 fois par seconde :)

## Conclusion

Nous sommes fin prêt à passer à la suite du projet.
Noter que vous pouvez suivre l'avancement sur le [Github](https://github.com/lchagnoleau/drone_software/tree/develop).