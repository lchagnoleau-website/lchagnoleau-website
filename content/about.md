+++
title = "Loïc Chagnoleau"
+++

{{< image src="/propos/moi.png" alt="Loïc Chagnoleau" position="center" style="border-radius: 8px;" >}}

Bonjour, je m'appelle Loïc et je suis ingénieur en systèmes embarqués. J'ai à cœur de partager ma passion et de prouver que ce domaine est accessible à tous. Nous réaliserons ensemble sur ce blog, des projets ambitieux. Il y aura également des articles hors projet mais toujours liés au même domaine.

J'ai décidé d'ouvrir ce blog et de m'imposer un calendrier (un article par semaine) pour augmenter ma créativé, mon expertise, ma pédagogie et bien sûr, pour partager mon experience en espérant qu'elle soit profitable à au moins certains d'entre vous.

Je suis convaincu que :

- La perfection s'obtient par la simplicité
- La curiosité est la plus grande des qualités
